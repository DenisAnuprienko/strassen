#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>
#include <mpi.h>

// Operations with matrices are performed
// as operations with parts of the parent matrices.
// For that we need to know the pointer to 
// the submatrix, its size and parent matrix size



int N;
double *A, *B, *C;

int taskid, numtasks, len;
char hostname[MPI_MAX_PROCESSOR_NAME];


// the following functions take arguments
// 1) n      -- submatrix size for result
// 2) n_*    -- parent matrix size
// 3) a      -- pointer to the submatrix

void print_mtr(int n, double *a, int n_a);
void add_mtr(int n, double *a, int n_a, double *b, int n_b, double *res, int n_res);
void sub_mtr(int n, double *a, int n_a, double *b, int n_b, double *res, int n_res);
void mul_mtr(int n, double *a, int n_a, double *b, int n_b, double *res, int n_res);

void copy(int n, double *a, int n_a, double *dest);

void strassen(int n, double *a, int n_a, double *b, int n_b, double *res, int n_res);

void init(void);
int  check(void);


int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &taskid);
    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

    if(argc != 2)
    {
        printf("Args error\n");
        return 1;
    }
    N = atoi(argv[1]);
    
    srand(time(NULL));
    
    double t1, t2;    

    if(taskid == 0)
    {
        printf("%d processes\n", numtasks);
        
        
        A = (double *)malloc(N*N * sizeof(double));
        B = (double *)malloc(N*N * sizeof(double));
        C = (double *)malloc(N*N * sizeof(double));
        
        init();
    
        t1 = omp_get_wtime();
        strassen(N, A, N, B, N, C, N);
        t2 = omp_get_wtime();
    }
    else
        strassen(N, NULL, N, NULL, N, NULL, N);
    
        
    if(taskid == 0)
    {
        printf("Strassen time: %lf\n", t2 - t1 );
        printf("check: %d\n", check());

        free(A);
        free(B);
        free(C);
    }
        
    MPI_Finalize();
}






void print_mtr(int n, double *a, int n_a)
{
    int i, j;
    for(i = 0; i < n; i ++)
    {
        for(j = 0; j < n; j ++)
        {
            printf("%lf ", a[i*n_a + j]);
        }
        printf("\n");
    }
    printf("\n");
}

void add_mtr(int n, double *a, int n_a, double *b, int n_b, double *res, int n_res)
{
    int i, j;
    for(i = 0; i < n; i ++)
        for(j = 0; j < n; j ++)
        {
            res[i*n_res + j] = a[i*n_a + j] + b[i*n_b + j];/////
        }
}

void sub_mtr(int n, double *a, int n_a, double *b, int n_b, double *res, int n_res)
{
    int i, j;
    for(i = 0; i < n; i ++)
        for(j = 0; j < n; j ++)
        {
            res[i*n_res + j] = a[i*n_a + j] - b[i*n_b + j];
        }
}



void mul_mtr(int n, double *a, int n_a, double *b, int n_b, double *res, int n_res)
{
    //cblas_dgemm(CblasRowMajor, 111, 111, n, n, n,
    //                1.0, A, n, B, n, 0, res, n);
    int i, j, k;
    for(i = 0; i < n; i ++)
    {
        for(j = 0; j < n; j ++)
        {
            res[i*n_res + j] = 0;
            for(k = 0; k < n; k ++)
            {
                res[i*n_res + j] += a[i*n_a + k] * b[k*n_b + j];
            }
        }
    }
}





void strassen(int n, double *a, int n_a, double *b, int n_b, double *res, int n_res)
{
    int i, n1 = n/2;
    
    double *m1, *m2, *m3, *m4, *m5, *m6, *m7;
    double *a11, *a12, *a21, *a22;
    double *b11, *b12, *b21, *b22;
    double *c11, *c12, *c21, *c22;
    double *t1, *t2;


    a11 = a;
    a12 = a + n1;
    a21 = a + n1*n_a;
    a22 = a + n1*n_a + n1;

    b11 = b;
    b12 = b + n1;
    b21 = b + n1*n_b;
    b22 = b + n1*n_b + n1;

    c11 = res;
    c12 = res + n1;
    c21 = res + n1*n_res;
    c22 = res + n1*n_res + n1;



    m1 = (double *)malloc(n1*n1 * sizeof(double));
    m2 = (double *)malloc(n1*n1 * sizeof(double));
    m3 = (double *)malloc(n1*n1 * sizeof(double));
    m4 = (double *)malloc(n1*n1 * sizeof(double));
    m5 = (double *)malloc(n1*n1 * sizeof(double));
    m6 = (double *)malloc(n1*n1 * sizeof(double));
    m7 = (double *)malloc(n1*n1 * sizeof(double));
    t1 = (double *)malloc(n1*n1 * sizeof(double));
    t2 = (double *)malloc(n1*n1 * sizeof(double));
   
    for(i = 0; i < n1*n1; i ++)
    {
        m1[i] = 0; m2[i] = 0; m3[i] = 0; m4[i] = 0; m5[i] = 0; m6[i] = 0; m7[i] = 0;
        t1[i] = 0; t2[i] = 0;
    }
    

    if(n <= N/2)
    {
        // M1 = (a11 + a22)(b11 + b22)
        add_mtr(n1, a11, n_a, a22, n_a, t1, n1);
        add_mtr(n1, b11, n_b, b22, n_b, t2, n1);
        mul_mtr(n1, t1, n1, t2, n1, m1, n1);
        
        // M2 = (a21 + a22)b11
        add_mtr(n1, a21, n_a, a22, n_a, t1, n1);
        mul_mtr(n1, t1,  n1,  b11, n_b, m2, n1);

        // M3 = a11(b12 - b22)
        sub_mtr(n1, b12, n_b, b22, n_b, t1, n1);
        mul_mtr(n1, a11, n_a, t1,  n1,  m3, n1);
        

        // M4 = a22(b21 - b11)
        sub_mtr(n1, b21, n_b, b11, n_b, t1, n1);
        mul_mtr(n1, a22, n_a, t1,  n1,  m4, n1);

        // M5 = (a11 + a12)b22
        add_mtr(n1, a11, n_a, a12, n_a, t1, n1);
        mul_mtr(n1, t1, n1, b22, n_b, m5, n1);

        // M6 = (a21 - a11)(b11 + b12)
        sub_mtr(n1, a21, n_a, a11, n_a, t1, n1);
        add_mtr(n1, b11, n_b, b12, n_b, t2, n1);
        mul_mtr(n1, t1, n1, t2, n1, m6, n1);

        // M7 = (a12 - a22)(b21 + b22)
        sub_mtr(n1, a12, n_a, a22, n_a, t1, n1);
        add_mtr(n1, b21, n_b, b22, n_b, t2, n1);
        mul_mtr(n1, t1, n1, t2, n1, m7, n1);
    }
    else
    {
        // This part is parallel.
        // Master does additions and subtractions, 
        // then sends factors to the workers.
        // They perform multiplication and send results back.

        if(taskid == 0) // Master

            // M1 = (a11 + a22)(b11 + b22)
            add_mtr(n1, a11, n_a, a22, n_a, t1, n1);
            add_mtr(n1, b11, n_b, b22, n_b, t2, n1);
            MPI_Send(t1, n1*n1, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD);
            MPI_Send(t2, n1*n1, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD);
            //strassen(n1, t1, n1, t2, n1, m1, n1);
        
            // M2 = (a21 + a22)b11
            add_mtr(n1, a21, n_a, a22, n_a, t1, n1);
            copy(n1, b11, n_b, t2);
            MPI_Send(t1, n1*n1, MPI_DOUBLE, 2, 0, MPI_COMM_WORLD);
            MPI_Send(t2, n1*n1, MPI_DOUBLE, 2, 0, MPI_COMM_WORLD);
            //strassen(n1, t1, n1, b11, n_b, m2, n1);

            // M3 = a11(b12 - b22)
            sub_mtr(n1, b12, n_b, b22, n_b, t2, n1);
            copy(n1, a11, n_a, t1);
            MPI_Send(t1, n1*n1, MPI_DOUBLE, 3, 0, MPI_COMM_WORLD);
            MPI_Send(t2, n1*n1, MPI_DOUBLE, 3, 0, MPI_COMM_WORLD);
            //strassen(n1, a11, n_a, t1, n1, m3, n1); // error with 'b', which is t1 here
        

            // M4 = a22(b21 - b11)
            sub_mtr(n1, b21, n_b, b11, n_b, t2, n1);
            copy(n1, a22, n_a, t1);
            MPI_Send(t1, n1*n1, MPI_DOUBLE, 4, 0, MPI_COMM_WORLD);
            MPI_Send(t2, n1*n1, MPI_DOUBLE, 4, 0, MPI_COMM_WORLD);
            //strassen(n1, a22, n_a, t1, n1, m4, n1);

            // M5 = (a11 + a12)b22
            add_mtr(n1, a11, n_a, a12, n_a, t1, n1);
            copy(n1, b22, n_b, t2);
            MPI_Send(t1, n1*n1, MPI_DOUBLE, 5, 0, MPI_COMM_WORLD);
            MPI_Send(t2, n1*n1, MPI_DOUBLE, 5, 0, MPI_COMM_WORLD);
            //strassen(n1, t1, n1, b22, n_b, m5, n1);

            // M6 = (a21 - a11)(b11 + b12)
            sub_mtr(n1, a21, n_a, a11, n_a, t1, n1);
            add_mtr(n1, b11, n_b, b12, n_b, t2, n1);
            MPI_Send(t1, n1*n1, MPI_DOUBLE, 6, 0, MPI_COMM_WORLD);
            MPI_Send(t2, n1*n1, MPI_DOUBLE, 6, 0, MPI_COMM_WORLD);
            //strassen(n1, t1, n1, t2, n1, m6, n1);

            // M7 = (a12 - a22)(b21 + b22)
            sub_mtr(n1, a12, n_a, a22, n_a, t1, n1);
            add_mtr(n1, b21, n_b, b22, n_b, t2, n1);
            MPI_Send(t1, n1*n1, MPI_DOUBLE, 7, 0, MPI_COMM_WORLD);
            MPI_Send(t2, n1*n1, MPI_DOUBLE, 7, 0, MPI_COMM_WORLD);
            //strassen(n1, t1, n1, t2, n1, m7, n1);
            
            //printf("Master sent all data\n");

            MPI_Recv(m1, n1*n1, MPI_DOUBLE, 1, 1, MPI_COMM_WORLD, NULL);
            MPI_Recv(m2, n1*n1, MPI_DOUBLE, 2, 2, MPI_COMM_WORLD, NULL);
            MPI_Recv(m3, n1*n1, MPI_DOUBLE, 3, 3, MPI_COMM_WORLD, NULL);
            MPI_Recv(m4, n1*n1, MPI_DOUBLE, 4, 4, MPI_COMM_WORLD, NULL);
            MPI_Recv(m5, n1*n1, MPI_DOUBLE, 5, 5, MPI_COMM_WORLD, NULL);
            MPI_Recv(m6, n1*n1, MPI_DOUBLE, 6, 6, MPI_COMM_WORLD, NULL);
            MPI_Recv(m7, n1*n1, MPI_DOUBLE, 7, 7, MPI_COMM_WORLD, NULL);
            
        }
        else
        {
            double start, finish;
            MPI_Recv(t1, n1*n1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, NULL);
            MPI_Recv(t2, n1*n1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, NULL);
            start = omp_get_wtime();
            strassen(n1, t1, n1, t2, n1, m1, n1);
            finish = omp_get_wtime();
            
            MPI_Send(m1, n1*n1, MPI_DOUBLE, 0, taskid, MPI_COMM_WORLD);
            printf("strassen of size %d in task %d took time: %lf\n", n1, taskid, finish - start);
        }
    }

        //==========================================//

    if(!(n == N && taskid != 0))
    {
        // C11 = M1 + M4 − M5 + M7
        add_mtr(n1, m1,  n1,    m4, n1, c11, n_res);   
        sub_mtr(n1, c11, n_res, m5, n1, c11, n_res);
        add_mtr(n1, c11, n_res, m7, n1, c11, n_res);

        // C12 = M3 + M5
        add_mtr(n1, m3, n1, m5, n1, c12, n_res);

        // C21 = M2 + M4
        add_mtr(n1, m2, n1, m4, n1, c21, n_res);

        // C22 = M1 − M2 + M3 + M6
        sub_mtr(n1, m1, n1, m2,  n1,    c22, n_res);
        add_mtr(n1, m3, n1, c22, n_res, c22, n_res);
        add_mtr(n1, m6, n1, c22, n_res, c22, n_res);
    }

    free(m1);
    free(m2);
    free(m3);
    free(m4);
    free(m5);
    free(m6);
    free(m7);
    free(t2);
    free(t1);
}

void init(void)
{
    int i, j;
    for(i = 0; i < N*N; i ++)
    {
        A[i] = rand();
        B[i] = 0;
    }
    for(i = 0; i < N; i ++)
        B[i*N + i] = 1;
    for(i = 0; i < N*N; i ++)
        C[i] = 0;
}


// This function checks if matrices A and C are equal
// after C = A*B, where B = I
int check(void)
{
    printf("============= Check =============\n");
    //print_mtr(N, C, N);
    int i;
    for(i = 0; i < N*N; i ++)
        if(C[i] != A[i]) return 1;
    return 0;
}


// This copies submatrix of size n from parent
// matrix of size n_a starting at 'a' to
// matrix 'dest' of size n

void copy(int n, double *a, int n_a, double *dest)
{
    int i, j;
    for(i = 0; i < n; i ++)
    {
        for(j = 0; j < n; j ++)
            dest[i*n + j] = a[i*n_a + j];
    }
}


